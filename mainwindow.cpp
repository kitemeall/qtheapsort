#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
//#include <iostream>
#include <QTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_FileOpenButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "~/Desktop/",
                "Text File (*.txt)"
                );
    ReadFromFile(filename);
}

void MainWindow::ReadFromFile(const QString &filePath)
{
    QFile mFile (filePath);

    if(!mFile.open(QFile::ReadOnly|QFile::Text))
    {
        ui->textEdit->setText("Bad file!");
        return;
    }
    QTextStream stream (&mFile);
    QString buffer = stream.readAll();
    ui->textEdit->setText(buffer);
}

bool MainWindow::StringValidator(const QString & str)
{
    int i=0;
    while(i<str.length()&&(str[i]>='0'&&str[i]<='9'||str[i]==' '||(str[i]=='-'&&str[i+1]>='0'&&str[i+1]<='9')||str[i]=='\n'||str[i]=='\t'))
        i++;
    return (i==str.length()&&str.length()!=0);
}

int MainWindow::NumberCount(const QString &str)
{
    int i=0;
    int n=0;
    while(i<str.length())
    {
        while(i<str.length()&&str[i]!=' '&&str[i]!='\n'&&str[i]!='\t')
            i++;
        n++;
        while(i<str.length()&&(str[i]==' '||str[i]=='\n'||str[i]=='\t'))
            i++;

    }
    return n;
}

void MainWindow:: FixDown(int a[], int k, int N)
{
    while (2 * k <= N)
    {
        int j = 2 * k;
        if (j < N && a[j] < a[j + 1])
            j++;
        if (!(a[k] < a[j]))
            break;
        std::swap(a[k], a[j]);
        k = j;
    }
}

void MainWindow::HeapSort(int a[], int l, int r)
{
    int k, N = r - l + 1;
    int *pq = a + l - 1;
    for (k = N / 2; k >= 1; k--)
        FixDown(pq, k, N);
    while (N > 1)
    {
        std::swap(pq[1], pq[N]);
        FixDown(pq, 1, --N);
    }
}

QString* MainWindow::ArrayToQstring(int *array, int size)
{
    QString* outputString= new QString;
    for(int i=0; i<size; i++)
    {
        (*outputString).append(QString::number(array[i]));
        (*outputString).append(" ");
    }
    return outputString;

}

void MainWindow::on_HeapSortButton_clicked()
{
    QString textAreaString= ui->textEdit->toPlainText();

    if(StringValidator(textAreaString))
    {
        arraySize=NumberCount(textAreaString);
        QTextStream stream(&textAreaString);
        delete [] arrayToSort;
        arrayToSort= new int [arraySize];
        for(int i=0; i<arraySize; i++) {
            stream >> arrayToSort[i];
        }
        QTime myTimer;
        myTimer.start();
        HeapSort(arrayToSort, 0, arraySize);
        int milliseconds = myTimer.elapsed();
        ui->TimeLine->setText(QString::number(milliseconds));
        QString *outputString= ArrayToQstring(arrayToSort, arraySize);
        ui->SortedArea->setPlainText((*outputString));
        delete outputString;
    }
    else
        ui->SortedArea->setText("Bad input!");

}

QString* MainWindow::GenerateInput(int min, int max, int count, bool reverce)
{
    QString *outputString= new QString;
    float curentVatlue=min;
    float addValue= (float)(max-min+1)/(float)count;
    if(reverce)
    {
        curentVatlue = max;
        addValue*=-1;
    }

    for(int i=0; i<count; i++)
    {
        (*outputString).append(QString::number((int)curentVatlue));
        (*outputString).append(" ");
        curentVatlue+=addValue;
    }





    return outputString;
}

void MainWindow::on_GenerateButton_clicked()
{
    int min= ui->MinValue->value();
    int max= ui->MaxValue->value();
    int count= ui->GenerateCount->value();
    bool reverce= ui->ReverceCheckBox->isChecked();
    QString* generatedString = GenerateInput(min, max, count, reverce);
    ui->textEdit->setPlainText((*generatedString));
    delete generatedString;
}

void MainWindow::on_MinValue_valueChanged(int arg1)
{
    if(ui->MaxValue->value()< arg1)
        ui->MaxValue->setValue(arg1);
}

void MainWindow::on_MaxValue_valueChanged(int arg1)
{
    if(ui->MinValue->value()> arg1)
        ui->MinValue->setValue(arg1);
}
