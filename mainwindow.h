#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void ReadFromFile(const QString &filePath);
    void HeapSort(int a[], int l, int r);
    ~MainWindow();

private slots:
    void on_FileOpenButton_clicked();

    void on_HeapSortButton_clicked();

    void on_GenerateButton_clicked();

    void on_MinValue_valueChanged(int arg1);

    void on_MaxValue_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;
    int * arrayToSort;
    int arraySize;
    bool StringValidator(const QString & str);
    int NumberCount(const QString &str);
    void FixDown(int a[], int k, int N);
    QString* ArrayToQstring(int *array, int size);
    QString* GenerateInput(int min, int max, int count, bool reverce);
};

#endif // MAINWINDOW_H
